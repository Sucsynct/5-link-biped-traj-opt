# 5-Link Biped Walk Cycle Trajectory Optimisation
## Abstract
Character animation is integral to video games, motion pictures, and other animated
media. Industry-standard methods for creating character animations are slow, expensive,
and often do not produce realistic results since the physics of the character are often not
a part of the animation process.

Physics-based character animation has been an active area of study for many
years, particularly in the last two decades. It may provide the solution to
creating physically-accurate, realistic-looking character animation without the
costs associated with industry-standard methods to achieve the same result.

This notebook details the approach I took to successfully create a simple,
physics-based biped walk-cycle using contact-invariant trajectory optimisation
through direct collocation.

The results generated from this notebook were then used to animate a
character armature using my [BuMP](https://gitlab.com/Sucsynct/bump) Blender script.

## Running
In order to run the notebook:

1. Ensure that any variation of the popular [Conda](https://docs.conda.io/en/latest/)
or [Mamba](https://mamba.readthedocs.io/en/latest/).
2. Clone the repository.
3. `cd` into the repository.
4. In a terminal run `mamba create -f env.yml` or `conda env create -f env.yml`
5. Activate the environment using `mamba activate trajopt` or `conda activate
   trajopt`.
6. Run `jupyter-lab` and open the notebook file in Jupyter's web interface.
